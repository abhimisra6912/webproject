/*package Hotwire_BusinessLogics;

public class BusinessLogics_Hotwire {

}*/
package Hotwire_BusinessLogics;
import java.awt.Robot;

import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.Action;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class BusinessLogics_Hotwire extends PageObjects_Hotwire.Hotwire_PageObjects {
	static WebDriver driver = null;
	ExtentTest logger;

	public BusinessLogics_Hotwire(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	/*
	 * opening url
	 */
	public void openUrl(String Url) throws InterruptedException
	{
		driver.get(Url);
		driver.manage().window().maximize();
		Thread.sleep(3000);
		
	}
/*
 * waiting for element implicitly
 */
		public boolean waitForElementToBeLoad(String xpath){ 
		boolean status = false;
		try {
			WebDriverWait waitObj = new WebDriverWait(driver, 60);
			waitObj.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		} catch (Exception e) {
			status = true;
			System.out.println("NO element found with Xpath : "+xpath);
		}
		return status;
	}
/*
 * Taking system date
 */
	public static String datetime(String format)
	{
		DateFormat dateFormat = new SimpleDateFormat(format);

        Date date = new Date();
        String todate = dateFormat.format(date);

        
        System.out.println(todate);
        return todate;
	}
/*
 * Taking screen shot
 */
	public void takeScreenShots() throws Exception {
		try {
			File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			String dateval = datetime("ddMMhhmmssSSS");
			String home = System.getProperty("user.dir");
			System.out.println("\n home:"+home);
			 // now copy the  screenshot to desired location using copyFile //method
			FileUtils.copyFile(src, new File(home+"\\resources\\screenshots\\"+dateval+".png"));
		} catch (Exception e) {
			System.out.println(e);
			throw new Exception(e.getMessage());
		}
	}
	/*
	 * Logging into the application
	 */
	
	
	
	
	/*
	 *validating if element is diaplayed
	 */
	public void validateElement(String xpath,String elementName) throws Exception {
			try {
					waitForElementToBeLoad(xpath);
				 if(driver.findElement(By.xpath(xpath)).isDisplayed())
				 {
					 takeScreenShots();
				 }
				 else
				 {
					 takeScreenShots();
					 Assert.fail(elementName+" is not present");
				 }
		} catch (Exception e) {
				System.out.println(e);
				throw new Exception(e.getMessage());
			}
		}
	/*
	 *validating logout
	 */
	public void Scroll(String xpath) throws Exception {
        try {
               WebElement ele = driver.findElement(By.xpath(xpath));
               JavascriptExecutor jse = (JavascriptExecutor) driver;
               ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView()", ele);
               jse.executeScript("return arguments[0].text", ele);       
        } catch (Exception e) {
               throw new Exception("scroll method failed");
        }
 }

	/*
	 * validating scroll
	 */
	
	
	public void validatingHotwire() throws Exception {
		try {
			 WebDriverWait wait = new WebDriverWait(driver, 60);
			 waitForElementToBeLoad("");
			 driver.findElement(By.xpath(lnkVaccation)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnCar)));
			 driver.findElement(By.xpath(btnCar)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(txtFlightFrom)));
			 driver.findElement(By.xpath(txtFlightFrom)).clear();
			 driver.findElement(By.xpath(txtFlightFrom)).sendKeys("SFO");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkSanFris)));
			 driver.findElement(By.xpath(lnkSanFris)).click();
			 System.out.println("San Fransisco Clicked");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(txtFlightTo)));
			 driver.findElement(By.xpath(txtFlightTo)).clear();
			 driver.findElement(By.xpath(txtFlightTo)).sendKeys("LAX");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkLosAng)));
			 driver.findElement(By.xpath(lnkLosAng)).click();
			 System.out.println("Los Angeles Clicked");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(dtStart)));
			 driver.findElement(By.xpath(dtStart)).click();
			 System.out.println("Start date Entered");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(dtStartDay)));
			 driver.findElement(By.xpath(dtStartDay)).click();
			 System.out.println("Start date Selected");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selPickTime)));
			 driver.findElement(By.xpath(selPickTime)).click();
			 System.out.println("Pick time selected");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selEve)));
			 driver.findElement(By.xpath(selEve)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(dtEnd)));
			 driver.findElement(By.xpath(dtEnd)).click();
			 System.out.println("End date Clicked");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnNextMonth)));
			 driver.findElement(By.xpath(btnNextMonth)).click();
			 System.out.println("Next Month Clicked");
			 /*wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selEve)));
			 driver.findElement(By.xpath(selEve)).click();*/
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(dtEndDay)));
			 driver.findElement(By.xpath(dtEndDay)).click();
			 System.out.println("End date Selected");
			 driver.findElement(By.xpath(btnSearch)).click();
			 System.out.println("Search Button clicked");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(txtHeader)));
			 String header = driver.findElement(By.xpath(txtHeader)).getText();
			 System.out.println(header);
			 Assert.assertEquals("Start by choosing your hotel", header);
			 System.out.println("Atleast one value is visible");
			 
			 
	} catch (Exception e) {
			System.out.println(e);
			throw new Exception(e.getMessage());
		}
	}
	
	public void validatingMSNWeather() throws Exception {
		try {
			 WebDriverWait wait = new WebDriverWait(driver, 60);
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			 /*//Find element by link text and store in variable "Element"        		
		     //WebElement Element = driver.findElement(By.xpath("//img[@alt='Download on the App Store']"));
		     //This will scroll the page till the element is found		
		     js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		     wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@id='follow-button']")));
		     driver.findElement(By.xpath("//a[@id='follow-button']")).click();*/
		     wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(currentTemp)));
			 String TempDelhi = driver.findElement(By.xpath(currentTemp)).getText();
			 System.out.println(TempDelhi);
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnArrow)));
			 driver.findElement(By.xpath(btnArrow)).click();
			 driver.findElement(By.xpath(txtSearch)).sendKeys("Bengaluru");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selBeng)));
			 driver.findElement(By.xpath(selBeng)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(currentTemp)));
			 String TempBeng = driver.findElement(By.xpath(currentTemp)).getText();
			 System.out.println(TempBeng);
			 int i=Integer.parseInt(TempDelhi);
			 System.out.println(i);
			 int j=Integer.parseInt(TempBeng);
			 System.out.println(j);
			 if((i<j)){
				 System.out.println("Delhi is colder");
			 }else{
				 System.out.println("Bengaluru is colder");
			 }
				 
	} catch (Exception e) {
			System.out.println(e);
			throw new Exception(e.getMessage());
		}
	}
	
	public void validatingMSNMoney() throws Exception {
		try {
			 WebDriverWait wait = new WebDriverWait(driver, 60);
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkWeather)));
			 driver.findElement(By.xpath(lnkWeather)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkMoney)));
			 driver.findElement(By.xpath(lnkMoney)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkCurrencyConv)));
			 driver.findElement(By.xpath(lnkCurrencyConv)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(drpdwnCountry)));
			 driver.findElement(By.xpath(drpdwnCountry)).click(); 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selAus)));
			 driver.findElement(By.xpath(selAus)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(txtAmt)));
			 driver.findElement(By.xpath(txtAmt)).clear();
			 driver.findElement(By.xpath(txtAmt)).sendKeys("4");
			 String element = driver.findElement(By.xpath(txtInd)).getText();
			 System.out.println(element);
			 Thread.sleep(5000);
			 
			 
			 
	} catch (Exception e) {
			System.out.println(e);
			throw new Exception(e.getMessage());
		}
	}
	
	public void validatingMSNTravel() throws Exception {
		try {
			 WebDriverWait wait = new WebDriverWait(driver, 60);
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkWeather)));
			 driver.findElement(By.xpath(lnkWeather)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkTravel)));
			 driver.findElement(By.xpath(lnkTravel)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkLifestyle)));
			 driver.findElement(By.xpath(lnkLifestyle)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(lnkFlightSearch)));
			 driver.findElement(By.xpath(lnkFlightSearch)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(txtDest)));
			 driver.findElement(By.xpath(txtDest)).sendKeys("Chennai");
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnDate)));
			 driver.findElement(By.xpath(btnDate)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selDate)));
			 driver.findElement(By.xpath(selDate)).click();
			 driver.findElement(By.xpath(btnRet)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnarrow)));
			 driver.findElement(By.xpath(btnarrow)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selRtnDate)));
			 driver.findElement(By.xpath(selRtnDate)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnMale)));
			 driver.findElement(By.xpath(btnMale)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selMale)));
			 driver.findElement(By.xpath(selMale)).click();
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnChild)));
			 driver.findElement(By.xpath(btnChild)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selChild)));
			 driver.findElement(By.xpath(selChild)).click();
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnInf)));
			 driver.findElement(By.xpath(btnInf)).click();
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(selInf)));
			 driver.findElement(By.xpath(selInf)).click();
			 
			 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(btnsearch)));
			 driver.findElement(By.xpath(btnsearch)).click();
			 
	} catch (Exception e) {
			System.out.println(e);
			throw new Exception(e.getMessage());
		}
	}
	
	

	public void clearingCatche() throws Exception {
		try {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.ups.com/in/en/Home.page");
		Thread.sleep(3000);
		driver.get("chrome://settings/clearBrowserData");
		Robot robot=new Robot();
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(10000);
		driver.close();
		} 
		catch (Exception e) {
			System.out.println(e);
			throw new Exception(e.getMessage());
		}
			
	}
	
	
	

}
