package PageObjects_Hotwire;

public class Hotwire_PageObjects {
	
/////Hotwire/////
	public static final String lnkVaccation = "(//a[contains(text(), 'Vacations')])[1]";
	public static final String btnCar = "//*[contains(@data-bdd,'farefinder-package-bundleoption-car')]";
	public static final String txtFlightFrom = "//input[@id='farefinder-package-origin-location-input']";
	public static final String lnkSanFris = "//a[contains(text(),'San Francisco, CA, United States of America (')]";
	public static final String txtFlightTo = "//input[@name='farefinder-package-destination-location']";
	public static final String lnkLosAng = "//a[@title='Los Angeles, CA, United States (LAX-Los Angeles Intl.)']";
	public static final String dtStart = "//input[@id='farefinder-package-startdate-input']";
	public static final String dtStartDay = "(//td[@ng-repeat='day in week']/div/div)[58]";
	public static final String selPickTime = "//select[@id='farefinder-package-pickuptime-input']";
	public static final String selEve = "(//option[@label='Evening'])[1]";
	public static final String dtEnd = "//input[@id='farefinder-package-enddate-input']";
	public static final String dtEndDay = "(//td[@ng-repeat='day in week']/div/div)[28]";
	public static final String btnSearch = "//button[@id='farefinder-package-search-button']";
	public static final String btnNextMonth = "(//button[@type='button' and @class='next'])[1]";
	public static final String txtHeader = "//h1[@class='section-header-main']";
	
////MSNWeather/////
	public static final String btnweather = "(//div[@class='mylocations']/div)[1]";
	public static final String btnArrow = "(//a[@role='button'])[1]";
	public static final String txtSearch = "(//input[@name='q'])[2]";
	public static final String selBeng = "//li[@id='locListItem0']";
	public static final String currentTemp = "//span[@class='current']";
	public static final String btnfollow = "//a[@id='follow-button']";
	
////MSNMoney//////	
	public static final String lnkWeather = "//a[contains(text(),'weather')]";
	public static final String lnkMoney = "//a[contains(text(),'Money')]";
	public static final String lnkCurrencyConv = "//*[@id='nav']/div/ul[1]/li[5]/a";
	public static final String drpdwnCountry = "//div[@id='frmlist']";
	public static final String selAus = "//*[@id='frmmenu']/option[6]";
	public static final String txtAmt = "//input[@id='frmtxtbx']";
	public static final String txtInd = "//*[@id='totxtbx']";
	
////MSNTravel/////
	public static final String lnkTravel = "(//a[contains(text(),'Travel')])[1]";
	public static final String lnkLifestyle = "//a[contains(text(),'lifestyle')]";
	public static final String lnkFlightSearch = "//a[contains(text(),'Flight Search')]";
	public static final String txtDest = "//a[@class='place-selector__cover text-ellipsis js-autocomplete-place-cover']";
	public static final String selDate = "(//div[@class='picker__day picker__day--infocus' and contains(text(),'27')])[1]";
	public static final String btnDate = "//*[@id='search-date-depart']/div[1]/button";
	public static final String btnRet = "//*[@class='ss-icon ss-icon-plus']";
	public static final String btnarrow = "(//*[@class='ss-icon-arrow-right ss-icon'])[2]";
	public static final String selRtnDate = "(//div[@class='picker__day picker__day--infocus' and contains(text(),'7')])[2]";
	public static final String btnMale = "//*[@aria-label='Number of passengers above 12 years old']";
	public static final String selMale = "//*[@id='people-and-class']/div[2]/div[1]/div/div/ul/li[2]/a";
	public static final String btnChild = "//*[@aria-label='Number of passengers under 12 years old']";
	public static final String selChild = "//*[@id='people-and-class']/div[2]/div[2]/div/div/ul/li[3]/a";
	public static final String btnInf = "//*[@aria-label='Number of passengers under 2 years old']";
	public static final String selInf = "//*[@id='people-and-class']/div[2]/div[4]/div/div/ul/li[3]/a";
	public static final String btnsearch = "//*[@class='field-box search-button js-search-button']";
	
	
	
	
	
	
	
	
	
	
	
	
	
}
