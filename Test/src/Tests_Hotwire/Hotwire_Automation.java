/*package Tests_Hotwire;

public class Hotwire_Automation {

}*/
package Tests_Hotwire;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class Hotwire_Automation {
	
	public static WebDriver bDriver = null;
	// Class Object list
	Hotwire_BusinessLogics.BusinessLogics_Hotwire bussi = null;
	//@BeforeTest
	public void setup() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\Chrome\\chromedriver.exe");
		bDriver = new ChromeDriver(); // for WebDriver - Chrome/IE/Firefox/Headless
		// Business Logic Object assignment
		bussi = new Hotwire_BusinessLogics.BusinessLogics_Hotwire(bDriver);
	}
	@Test
	public void TC_TestCaseName() throws Throwable {
		try {
			setup();
			
				bussi.openUrl("https://www.msn.com/en-in/weather/today/new-delhidlindia/we-city?iso=IN&el=FeSJ2PFuo8EidLLIeEk7qw%3D%3D&lat=28.610&long=77.201");
				//bussi.validatingMSNWeather();
				bussi.validatingMSNMoney();
				//bussi.validatingMSNTravel();
				//bussi.validatingHotwire();
				//bussi.clearingCatche();
			
			
			// TODO Test Case Step Function
		} catch (Exception e) {
			System.out.println(e);
		}
	}


	@AfterTest
	public void tearDown() {
		if (bDriver != null)
			  bDriver.close();
	}

}
